import tensorflow as tf

def weight_variable(shape, summary=False):
    with tf.name_scope('weight'):
        initial = tf.truncated_normal(shape, stddev=0.1)
        var = tf.Variable(initial, name='weight')
        if summary:
            attach_summaries(var)
    return var

def bias_variable(shape, summary=False):
    with tf.name_scope('bias'):
        initial = tf.constant(0.1, shape=shape)
        var = tf.Variable(initial, name='bias')
        if summary:
            attach_summaries(var)
    return var

def max_pool2d(layer, k=2):
    return tf.nn.max_pool(layer, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME')

def conv2d(layer, W, b):
    layer = tf.nn.conv2d(layer, W, strides=[1, 1, 1, 1], padding='SAME')
    layer = tf.nn.bias_add(layer, b)
    return tf.nn.relu(layer)

def conv_layer(layer, input_size, output_size, kernel=5, pool=True, summary=True):
    conv_weight = weight_variable([kernel, kernel, input_size, output_size], summary)
    conv_bias = bias_variable([output_size], summary)

    conv = conv2d(layer, conv_weight, conv_bias)
    if pool:
        return max_pool2d(conv)
    return conv

def attach_summaries(var):
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.scalar('max', tf.reduce_max(var))
        tf.summary.scalar('min', tf.reduce_min(var))
        tf.summary.histogram('histogram', var)
