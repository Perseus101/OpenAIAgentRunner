class Agent(object):
    def __init__(self, action_space, obs_space):
        self.action_space = action_space
        self.obs_space = obs_space
        self.reset()

    def act(self, observation):
        raise NotImplemented('Action must be implemented by subclass')

    def train(self, prev_ob, action, ob, reward):
        pass

    def save(self, step):
        pass

    def reset(self):
        pass
