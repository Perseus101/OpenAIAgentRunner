from .agentinfo import AgentInfo
from .agent import Agent

agent_info = AgentInfo('Keyboard Agent', 'keyboard', lambda *largs, **kargs: KeyboardAgent(*largs, **kargs))

class KeyboardAgent(Agent):
    def __init__(self, action_space, obs_space):
        super(KeyboardAgent, self).__init__(action_space, obs_space)
        self.action = 0
        self.pause = False

    def key_press(self, key, mod):
        if key==32:
            self.pause = not self.pause
        a = int( key - ord('0') )
        if a <= 0 or a >= self.action_space.n:
            return
        self.action = a
    def key_release(self, key, mod):
        a = int( key - ord('0') )
        if a <= 0 or a >= self.action_space.n:
            return
        if self.action == a:
            self.action = 0

    def act(self, observation):
        return self.action
