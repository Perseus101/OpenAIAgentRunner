import numpy as np
import tensorflow as tf

from .agentinfo import AgentInfo
from .tfagent import TFAgent
from .tfutil import *

agent_info = AgentInfo('Convolutional Neural Network Agent', 'cnn', lambda *largs, **kargs: CNNAgent(*largs, **kargs))

class CNNAgent(TFAgent):
    def __init__(self, action_space, obs_space, n_neurons=128, dropout = 0.7, discount=0.9, **kargs):
        self.n_neurons = n_neurons
        self.dropout = dropout
        self.discount = discount

        super(CNNAgent, self).__init__(action_space, obs_space, **kargs)

    def create_network(self):
        with tf.name_scope('input'):
            self.obs = tf.placeholder(tf.float32, self.obs_space)
            self.next_q = tf.placeholder(tf.float32, [1, self.action_space.n])
            self.keep_prob = tf.placeholder(tf.float32)

        with tf.name_scope('output'):
            self.q_out = self._net()
            self.action = tf.argmax(self.q_out, 1)

        with tf.name_scope('optimization'):
            self.loss = tf.reduce_sum(tf.square(self.next_q - self.q_out))
            tf.summary.scalar('loss', self.loss)
            self.global_step = tf.Variable(0, name='global_step', trainable=False)
            self.optimizer = tf.train.AdamOptimizer().minimize(self.loss, global_step=self.global_step)

    def _net(self):
        with tf.name_scope('conv1'):
            conv1 = conv_layer(self.obs, self.obs_space[-1], 16, pool=False)
        with tf.name_scope('conv2'):
            conv2 = conv_layer(conv1, 16, 32, pool=True)
        with tf.name_scope('conv3'):
            conv3 = conv_layer(conv2, 32, 64, pool=False)
        with tf.name_scope('conv4'):
            conv4 = conv_layer(conv3, 64, 128, pool=False)
            pool4 = max_pool2d(conv4, k=5)
            pool_flat = tf.reshape(pool4, [-1, np.prod(pool4.get_shape().as_list()[1:])])

        with tf.name_scope('fc1'):
            fc1_weight = weight_variable([pool_flat.get_shape().as_list()[-1], self.n_neurons])
            fc1_bias   = bias_variable([self.n_neurons])
 
            fc1 = tf.nn.relu(tf.add(tf.matmul(pool_flat, fc1_weight), fc1_bias))
            fc1_drop = tf.nn.dropout(fc1, self.keep_prob)
        with tf.name_scope('fc2'):
            fc2_weight = weight_variable([self.n_neurons, self.action_space.n])
            fc2_bias   = bias_variable([self.action_space.n])

            fc2 = tf.add(tf.matmul(fc1_drop, fc2_weight), fc2_bias)
        return fc2

    def act(self, observation):
        return self.sess.run(self.action, feed_dict={self.obs:observation, self.keep_prob:1.})[0]

    def train(self, prev_ob, action, ob, reward):
        old_q = self.sess.run(self.q_out, feed_dict={self.obs:prev_ob, self.keep_prob:1.})
        new_q = self.sess.run(self.q_out, feed_dict={self.obs:ob, self.keep_prob:1.})
        max_q = np.max(new_q)
        target_q = old_q
        target_q[0, action] = reward + self.discount*max_q
        _, summary, step = self.sess.run([self.optimizer, self.summary, self.global_step], feed_dict={self.obs:prev_ob, self.next_q:target_q, self.keep_prob:self.dropout})
        super(CNNAgent, self).write_summary(summary, step)
