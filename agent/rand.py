from .agentinfo import AgentInfo
from .agent import Agent

agent_info = AgentInfo('Random Agent', 'rand', lambda *largs, **kargs: RandomAgent(*largs, **kargs))

class RandomAgent(Agent):
    def __init__(self, action_space, obs_space):
        super(RandomAgent, self).__init__(action_space, obs_space)

    def act(self, observation):
        return self.action_space.sample()
