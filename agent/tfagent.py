import tensorflow as tf
import atexit
import os

from .agent import Agent

class TFAgent(Agent):
    def __init__(self, action_space, obs_space, save_dir='model', load=True):
        super(TFAgent, self).__init__(action_space, obs_space)
        self.save_dir = save_dir
        self.create_network()

        self.sess = tf.Session()
        atexit.register(self.sess.close)

        self.saver = tf.train.Saver()
        self.summary_writer = tf.summary.FileWriter(os.path.join(save_dir, 'summary'), self.sess.graph)
        self.summary = tf.summary.merge_all()
        try:
            if not load:
                raise
            ckpt = tf.train.get_checkpoint_state(save_dir)
            self.saver.restore(self.sess, ckpt.model_checkpoint_path)
        except:
            print('Creating new model.')
            self.sess.run(tf.global_variables_initializer())

    def create_network(self):
        raise NotImplemented('Network creation must be implemented by subclass')

    def write_summary(self, summary, step):
        self.summary_writer.add_summary(summary, step)

    def save(self, step):
        self.saver.save(self.sess, os.path.join(self.save_dir, 'model.ckpt'), step)
