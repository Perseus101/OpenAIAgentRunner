import collections

class AgentInfo(collections.namedtuple('AgentInfo', ['name', 'arg_name', 'init'])):
    """
    Describes an agent.

    Attributes:
        name:
            A common name for the agent. Should be simple and human readable.
        arg_name:
            A short name for the agent. This name may not have spaces.
            This identifier is used on the command line when selecting which agent to use.
        init:
            The initialization function for the agent. This function should return an instance of the agent.
    """
