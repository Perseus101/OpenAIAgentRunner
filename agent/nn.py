import numpy as np
import tensorflow as tf

from .agentinfo import AgentInfo
from .tfagent import TFAgent
from .tfutil import *

agent_info = AgentInfo('Neural Network Agent', 'nn', lambda *largs, **kargs: NNAgent(*largs, **kargs))

class NNAgent(TFAgent):
    def __init__(self, action_space, obs_space, n_neurons=64, dropout = 0.7, discount=0.9, learning_rate=0.0001, **kargs):
        self.n_neurons = n_neurons
        self.dropout = dropout
        self.discount = discount
        self.learning_rate = learning_rate

        super(NNAgent, self).__init__(action_space, obs_space, **kargs)

    def create_network(self):
        with tf.name_scope('input'):
            self.obs = tf.placeholder(tf.float32, self.obs_space)
            self.next_q = tf.placeholder(tf.float32, [1, self.action_space.n])
            self.keep_prob = tf.placeholder(tf.float32)

        with tf.name_scope('output'):
            self.q_out = self._net()
            self.action = tf.argmax(self.q_out, 1)

        with tf.name_scope('optimization'):
            tf.summary.histogram('q_out', self.q_out)
            self.max_q = tf.reduce_max(self.q_out, reduction_indices=[1])[0]
            tf.summary.scalar('max_q', self.max_q)
            self.loss = tf.reduce_sum(tf.square(self.next_q - self.q_out))
            tf.summary.scalar('loss', self.loss)
            self.global_step = tf.Variable(0, name='global_step', trainable=False)
            self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss, global_step=self.global_step)

    def _net(self):
        with tf.name_scope('fc1'):
            fc1_weight = weight_variable([self.obs_space[-1], self.n_neurons])
            fc1_bias   = bias_variable([self.n_neurons])

            fc1 = tf.nn.tanh(tf.add(tf.matmul(self.obs, fc1_weight), fc1_bias))

        with tf.name_scope('fc2'):
            fc2_weight = weight_variable([self.n_neurons, self.n_neurons])
            fc2_bias   = bias_variable([self.n_neurons])
            fc2 = tf.nn.tanh(tf.add(tf.matmul(fc1, fc2_weight), fc2_bias))

        with tf.name_scope('fc3'):
            fc3_weight = weight_variable([self.n_neurons, self.action_space.n])
            fc3_bias   = bias_variable([self.action_space.n])

            fc3 = tf.add(tf.matmul(fc2, fc3_weight), fc3_bias)
        return fc3

    def act(self, observation):
        return self.sess.run(self.action, feed_dict={self.obs:observation, self.keep_prob:1.})[0]

    def train(self, prev_ob, action, ob, reward):
        q = self.sess.run(self.q_out, feed_dict={self.obs:prev_ob, self.keep_prob:1.})
        max_q = self.sess.run(self.max_q, feed_dict={self.obs:ob, self.keep_prob:1.})
        q[0, action] = reward + self.discount*max_q
        _, summary, step = self.sess.run([self.optimizer, self.summary, self.global_step], feed_dict={self.obs:prev_ob, self.next_q:q, self.keep_prob:self.dropout})
        super(NNAgent, self).write_summary(summary, step)
