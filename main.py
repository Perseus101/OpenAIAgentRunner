import logging_setup

import gym
from gym.spaces import Box, Discrete
from gym import wrappers

import numpy as np

import argparse
import time

import agent_presets
from episode_record import EpisodeRecord
from agent.keyboard import KeyboardAgent

import logging

logger = logging.getLogger(__name__)

def main(args):
    logger.info('Creating environment {}'.format(args.env_id))
    env = gym.make(args.env_id)
    if isinstance(env.observation_space, Box):
        map_obs = lambda x: [x]
        obs_shape = (1,) + env.observation_space.shape
    elif isinstance(env.observation_space, Discrete):
        map_obs = lambda x: np.identity(env.observation_space.n)[x:x+1]
        obs_shape = (1, env.observation_space.n)
    else:
        raise ValueError('Unable to map environment {}'.format(args.env_id))

    agent = args.info.init(env.action_space, obs_shape, **args.extra)

    outdir = '/tmp/agent'
    env = wrappers.Monitor(env, directory=outdir, force=True)
    env.seed(0)

    episode_count = args.num_episodes
    randomness = args.randomness
    rand_decay = args.random_decay

    for i in range(episode_count):
        if i % args.save_interval == 0:
            agent.save(step=i)
        i += 1
        ob = env.reset()

        if isinstance(agent, KeyboardAgent):
            env.render()
            env.unwrapped.viewer.window.on_key_press = agent.key_press
            env.unwrapped.viewer.window.on_key_release = agent.key_release

        agent.reset()
        step = -1
        record = EpisodeRecord()
        while True:
            action = agent.act(map_obs(ob))
            if np.random.rand(1) < randomness:
                action = env.action_space.sample()

            prev_ob = ob
            ob, reward, done, info = env.step(action)
            agent.train(map_obs(prev_ob), action, map_obs(ob), reward)
            record.append(map_obs(prev_ob), map_obs(ob), reward, action)

            if args.render and i % args.render_interval == 0:
                env.render()
                if args.sleep_interval != None:
                    time.sleep(args.sleep_interval)

            step += 1
            if done:
                record.train_replay(agent)
                randomness *= rand_decay
                if i % args.print_interval == 0:
                    logger.info('Episode {}:\n\tReward: {}\n\tSteps: {}'.format(i, record.total_reward(), step))
                break

    env.close()
    if args.upload:
        logger.info('Uploading results stored in {}'.format(outdir))
        gym.upload(outdir)

def parse_model_args(args):
    if len(args) % 2 != 0:
        raise ValueError('Invalid number of extra arguments')
    extra = {}
    for i, key in enumerate(args):
        if (i+1) % 2 == 0:
            continue
        if not args[i].startswith('--'):
            raise ValueError('Keys must start with --')
        key = key[2:]
        value = args[i+1]
        if value == 'True':
            value = True
        elif value == 'False':
            value = False
        elif value.isdigit():
            value = int(value)
        extra[key] = value
    return extra

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='OpenAI gym runner')
    parser.add_argument('-e', '--env_id', required=True, help='Select the environment to run')
    parser.add_argument('-n','--num-episodes', default=10000, type=int, help='The number of episodes to run.')
    parser.add_argument('-r','--render', action='store_true', help='Render the environment?')
    parser.add_argument('-u','--upload', action='store_true', help='Should results be uploaded?')
    parser.add_argument('--print-interval', default=1, type=int, help='The number of episodes to run between printing run information.')
    parser.add_argument('--render-interval', default=1, type=int, help='The number of episodes to run between rendering episodes.')
    parser.add_argument('--save-interval', default=10, type=int, help='The number of episodes to run between saves.')
    parser.add_argument('--sleep-interval', default=None, type=float, help='The time in seconds to sleep between frames when rendering.')
    parser.add_argument('--randomness', default=0.9, type=float, help='Percent chance to take a random action.')
    parser.add_argument('--random-decay', default=0.999, type=float, help='Decay rate of randomness.')

    subparsers = parser.add_subparsers(help='Agent Types:')
    for agent_info in agent_presets.agents:
        subparsers.add_parser(agent_info.arg_name, help=agent_info.name).set_defaults(info=agent_info)

    args, unknown = parser.parse_known_args()

    args.extra = parse_model_args(unknown)
    main(args)
