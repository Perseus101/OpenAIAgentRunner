import unittest
from unittest.mock import Mock, call

from ..episode_record import EpisodeRecord
from ..agent.agent import Agent

class EpisodeRecordTest(unittest.TestCase):
    def setUp(self):
        self.record = EpisodeRecord()
        self.record.append([1, 0, 0, 0], 1, 0)
        self.record.append([0, 1, 0, 0], 2, 0)
        self.record.append([0, 0, 1, 0], 3, 0)
        self.record.append([0, 0, 0, 1], 4, 0)
        self.record.append([0, 0, 0, 0], 5, 0)

    def test_append(self):
        self.assertEquals(5, len(self.record.observations))
        self.assertEquals(5, len(self.record.rewards))
        self.assertEquals(5, len(self.record.actions))

    def test_total_reward(self):
        self.assertEquals(15, self.record.total_reward())

    def test_train_replay(self):
        fake_agent = Agent(None, None)
        fake_agent.train = Mock()
        self.record.train_replay(fake_agent)
        calls = [call([1, 0, 0, 0], 0, None, 1, max_q=14),
                call([0, 1, 0, 0], 0, None, 2, max_q=12),
                call([0, 0, 1, 0], 0, None, 3, max_q=9),
                call([0, 0, 0, 1], 0, None, 4, max_q=5),
                call([0, 0, 0, 0], 0, None, 5, max_q=0)]
        fake_agent.train.assert_has_calls(calls)

if __name__ == '__main__':
    unittest.main()
