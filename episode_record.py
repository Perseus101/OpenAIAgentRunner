import numpy as np
import random

class EpisodeRecord:
    def __init__(self):
        self.prev_observations = []
        self.observations = []
        self.rewards = []
        self.actions = []

    def append(self, prev_ob, ob, reward, action):
        self.prev_observations.append(prev_ob)
        self.observations.append(ob)
        self.rewards.append(reward)
        self.actions.append(action)

    def total_reward(self):
        return np.sum(self.rewards)

    def train_replay(self, agent, percent_train = 0.3):
        samples = random.sample(range(0, len(self.rewards)), int(len(self.rewards) * percent_train))
        for prev_ob, ob, reward, action in zip([self.prev_observations[i] for i in samples],
                [self.observations[i] for i in samples],
                [self.rewards[i] for i in samples],
                [self.actions[i] for i in samples]):
            agent.train(prev_ob, action, ob, reward)
