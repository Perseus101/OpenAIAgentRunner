import pkgutil
import agent

pkg_ignore = ['agent', 'agentinfo', 'tfagent', 'tfutil']
_prefix = agent.__name__ + '.'
pkg_ignore = [ _prefix + name for name in pkg_ignore ]

agents = [ __import__(modname, fromlist='dummy').agent_info for _, modname, ispkg in pkgutil.iter_modules(agent.__path__, _prefix) if not(modname in pkg_ignore or ispkg) ]
