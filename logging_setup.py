import os
import logging.config
os.environ['GYM_NO_LOGGER_SETUP'] = 'disable'
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '[%(levelname)s] %(name)s: %(message)s'
        },
        'verbose': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
        'file': {
            'level': 'INFO',
            'formatter': 'verbose',
            'filename': 'output.log',
            'class': 'logging.FileHandler',
        },
        'verbose_file': {
            'level': 'DEBUG',
            'formatter': 'verbose',
            'filename': 'verbose_output.log',
            'class': 'logging.FileHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console', 'file', 'verbose_file'],
            'level': 'DEBUG',
            'propagate': True
        },
    }
})
